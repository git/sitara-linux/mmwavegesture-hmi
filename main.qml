/*
* Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

import QtQuick 2.9
import QtQuick.Window 2.2

Window{
    id:mainWindow
    visible: true
    width: 1920
    height: 1080
    title: qsTr("mmWave + Sitara Gesture Control Demo")
    color: "black"
    property string mysource
    property bool mypaused : true
    property bool myvisibility: false

	Text {
        text: "Enter 4 digits password 6843 to open the lock. " +
              "Use up->down gesture to increment the counter of current highlighted digit. " +
              "Use right->left gesture to move to next right digit index.\n"
		font.family: "Helvetica"
        font.pointSize: 20
        fontSizeMode: Text.Fit
        width: parent.width*0.7
        wrapMode: Text.Wrap
        color: "yellow"
        x: parent.width*0.15
        y: parent.height*0.85
	}

    Digit { id: digit1; visible: myvisibility; objectName: "myDigit1"; x:parent.width*0.275; y:parent.height * 0.65; size:parent.width*0.09}
    Digit { id: digit2; visible: myvisibility; objectName: "myDigit2"; anchors.left:digit1.right; anchors.leftMargin:0.033*parent.width; y:digit1.y; size:digit1.size}
    Digit { id: digit3; visible: myvisibility; objectName: "myDigit3"; anchors.left:digit2.right; anchors.leftMargin:0.033*parent.width; y:digit1.y; size:digit1.size}
    Digit { id: digit4; visible: myvisibility; objectName: "myDigit4"; anchors.left:digit3.right; anchors.leftMargin:0.033*parent.width; y:digit1.y; size:digit1.size}

    property int activeid: 0

    AnimatedImage{
        id:lock
        visible: myvisibility
        source: mainWindow.mysource
        width:parent.width/2; height: parent.height/2
        property int idx: 1
        x:(parent.width-width)/2
        y: parent.height*0.095// (parent.height-height)/4
        paused: mainWindow.mypaused
        onFrameChanged: if(currentFrame==frameCount-1) { mainWindow.mypaused = true; }
    }
}
