/*
* Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QCoreApplication>
#include <QDebug>
#include <QTimer>
#include "keyword.h"

QT_USE_NAMESPACE

Keyword::Keyword(QObject *qmlCompObject)
{
    m_digitArray[0] = qmlCompObject->findChild<QObject *>("myDigit1");
    m_digitArray[1] = qmlCompObject->findChild<QObject *>("myDigit2");
    m_digitArray[2] = qmlCompObject->findChild<QObject *>("myDigit3");
    m_digitArray[3] = qmlCompObject->findChild<QObject *>("myDigit4");

    m_qmlCompObject = qmlCompObject;

    //initialize the displayed digits as 3337
    for (int i = 0; i < 3; i++){
        m_counter[i] = 0;
    }

    m_counter[3] = 5;

    /* Substract the keyword with 3 as the keyword is matched to the "model" index and not
     * the value at that index. Check the PathView in Digit.qml to understand model. A PathView
     * displays data from models created from built-in QML types.  A model provides a
     * set of data that is used to create the items for the view.  To avoid many scrolls to
     * to set the password, password is restricted between 3 and 8 and mmWave part number is
     * 6843, which is set as initial password*/
    m_keyword[0] = 3;//6 - 3;
    m_keyword[1] = 5;//8 - 3;
    m_keyword[2] = 1;//4 - 3;
    m_keyword[3] = 0;//3 - 3;

    qDebug() <<  " keyword index" << m_keyword[0] << m_keyword[1] << m_keyword[2] << m_keyword[3];
    m_activeIndex = 0;

    m_digitArray[m_activeIndex]->setProperty("borderColor", "red");
    m_digitArray[m_activeIndex]->setProperty("borderWidth", 9);
    m_digitArray[m_activeIndex]->setProperty("digit", m_counter[0]);

    for (int i = 1; i < 4; i++){
        if(m_digitArray[i]){
            m_digitArray[i]->setProperty("borderColor", "black");
            m_digitArray[i]->setProperty("borderWidth", 1);
            m_digitArray[i]->setProperty("digit", m_counter[i]);
        }
    }

    m_numtimes_keyword_match = 1;
}

Keyword::~Keyword()
{
    for (int i = 0; i < 4 ; i++){
        m_digitArray[i] = nullptr;
        m_counter[i] = 0;
    }
}

void Keyword::emitKeywordQuit()
{
    qDebug() << "Emit keywordQuit signal\n";
    emit keywordQuit();
}

void Keyword::handleDownGesture()
{
    bool keyMatch = false;
    m_counter[m_activeIndex] = (m_counter[m_activeIndex] + 1) % 6;

    m_digitArray[m_activeIndex]->setProperty("digit", m_counter[m_activeIndex]);

    keyMatch = ((m_digitArray[0]->property("digit") == m_keyword[0]) &&
            (m_digitArray[1]->property("digit") == m_keyword[1]) &&
            (m_digitArray[2]->property("digit") == m_keyword[2]) &&
            (m_digitArray[3]->property("digit") == m_keyword[3]));

    //Refresh the screen with updated digit
    QTimer::singleShot(0, m_qmlCompObject, SLOT(update()));

    if (keyMatch == true){
        m_qmlCompObject->setProperty("mypaused", false);
        m_numtimes_keyword_match -= 1;
        if(m_numtimes_keyword_match <= 0){
            QTimer::singleShot(5000, this, SLOT(emitKeywordQuit()));
        }
    }
}

void Keyword::handleRightGesture()
{
    m_digitArray[m_activeIndex]->setProperty("borderColor", "black");
    m_digitArray[m_activeIndex]->setProperty("borderWidth", 2);
    m_activeIndex = (m_activeIndex + 1) % 4;

    m_digitArray[m_activeIndex]->setProperty("borderColor", "red");
    m_digitArray[m_activeIndex]->setProperty("borderWidth", 9);

    //Refresh the screen with updated digit index
    QTimer::singleShot(0, m_qmlCompObject, SLOT(update()));
}

void Keyword::handleMotionDetected()
{
    m_qmlCompObject->setProperty("color", "gray");
    m_qmlCompObject->setProperty("myvisibility", true);
}

void Keyword::handleNoActivity()
{
    m_qmlCompObject->setProperty("color", "black");
    m_qmlCompObject->setProperty("myvisibility", false);
}
