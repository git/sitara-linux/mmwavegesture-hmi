/*
* Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QGuiApplication>
#include <QQmlEngine>
#include <qqml.h>
#include <QQmlComponent>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QQmlContext>
#include <QScreen>

#include "keyword.h"
#include "serialreaderthread.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlEngine engine;
    QQmlComponent component(&engine, QUrl("qrc:/main.qml"));
    QObject *object = component.create();

    printf("Usage: mmwavegesture-hmi <# of iterations to open the lock> -platform eglfs\n");
    printf("Default settings: # of iterations to open the lock = 1;  password = 6843\n");

    if(object == NULL){
        qDebug() << "Cannot create root object\n";
        return -1;
    }

    /* Set the main window property like image name, displayed width and height of the image */
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    int width = screenGeometry.width();
    int height = screenGeometry.height();
    object->setProperty("width", width);
    object->setProperty("height", height);
    object->setProperty("mysource", "lock_unlock.gif");

    /* Create Keyword and gestureReaderThread object. Connect the signals emitted by gestureRaederThread  with the slots in the Keyword object */
    Keyword keyword(object);
    SerialReaderThread *gestureReaderThread;
    gestureReaderThread = new SerialReaderThread();

    /*Connect the signals and slots between gestureRaederThread and  Keyword object */
    QObject::connect(gestureReaderThread, &SerialReaderThread::downGesture, &keyword, &Keyword::handleDownGesture, Qt::BlockingQueuedConnection );
    QObject::connect(gestureReaderThread, &SerialReaderThread::rightGesture, &keyword, &Keyword::handleRightGesture, Qt::BlockingQueuedConnection );
    QObject::connect(gestureReaderThread, &SerialReaderThread::motionDetected, &keyword, &Keyword::handleMotionDetected, Qt::BlockingQueuedConnection );
    QObject::connect(gestureReaderThread, &SerialReaderThread::noActivity, &keyword, &Keyword::handleNoActivity, Qt::BlockingQueuedConnection );
    QObject::connect(&keyword, &Keyword::keywordQuit, gestureReaderThread, &SerialReaderThread::handleQuit, Qt::QueuedConnection);
    QObject::connect(gestureReaderThread, &SerialReaderThread::srtQuit, &app, &QGuiApplication::quit, Qt::QueuedConnection);

    if(argc > 1){
        keyword.m_numtimes_keyword_match = atoi(argv[1]);
        qDebug() << "number of iterations to open the lock: " <<  keyword.m_numtimes_keyword_match;
    }

    /* Start thread that reads the gesture from mmWave Sensor and take predetermined action per the gestures received */
    gestureReaderThread->start();

    app.exec();

    qDebug() << "QGuiApplication execution over\n";

    return 0;
}

