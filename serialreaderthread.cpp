/*
* Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QDebug>
#include <termios.h>
#include "serialreaderthread.h"
#include "mmwgesturein.h"

SerialReaderThread::SerialReaderThread()
{
    m_privmode = GESTURE_MODE_IDLE;
    m_runSerialThread = true;
}

SerialReaderThread::~SerialReaderThread(void)
{
    qDebug() << "SerailReaderThread object is being deleted\n";
    m_serialPort->close();
    delete m_serialPort;
}

int SerialReaderThread::createSerialPort()
{
    /* Open the serial port. Serail port property gets set by SerialReaderThread Object */
    m_serialPort = new QSerialPort;
    QString serialPortName = "/dev/ttyACM1";// "XDS110 Class Auxiliary Data Port";
    m_serialPort->setPortName(serialPortName);

    if (!m_serialPort->open(QIODevice::ReadOnly)) {
        qDebug() << QObject::tr("Failed to open port %1, error: %2").arg(serialPortName).arg(m_serialPort->errorString()) << endl;
        return -1;
}

    m_serialPort->setBaudRate(921600, QSerialPort::Input);
    m_serialPort->setDataBits(QSerialPort::Data8);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);

    qDebug() << "Serial port opened successfully";
    return 1;
}

void SerialReaderThread::handleQuit()
{
    m_runSerialThread = false;
}

void SerialReaderThread::run()
{
    printf("inside serialthread run\n");
    //Need to create serialport in this thread else QT application throws error.
    // QT doesn't allow QSocket to be accessed from two different threads (main thread and serial thread)
    // Get inside while loop and attempt to read the bytes only if the serial port was opened successfully
    if(createSerialPort() > 0){
        while(m_runSerialThread == true){
        if ( m_serialPort->bytesAvailable() > 0 || m_serialPort->waitForReadyRead(10))
        {
            QByteArray ba;
            ba=m_serialPort->readAll();

            //Burst size is of 8 bytes. Less than 8 bytes is not useful to parse\n");
            if(ba.size() < 8){
                continue;
            }


            //parse further if magic word 0x12, 0x34, 0x56, 0x78 is found
            if (((unsigned char)ba[0] == 0x12) && ((unsigned char)ba[1] == 0x34) && (
                        (unsigned char)ba[2] == 0x56) && ((unsigned char)ba[3] == 0x78)){

                //parse for error bits. Process further only if there's no error in the mode info
                if(! (ba[4] & GESTURE_ERROR_BIT_MASK)){

                    Gesture_mode_type mode_info = (Gesture_mode_type)((ba[4] & (GESTURE_MODE_BIT_MASK)) >> GESTURE_MODE_BIT_SHIFT);

                    switch(mode_info){
                    case GESTURE_MODE_IDLE: if (m_privmode == GESTURE_MODE_GESTURE) emit noActivity();
                        m_privmode = GESTURE_MODE_IDLE;
                        break;
                    case GESTURE_MODE_GESTURE: if(m_privmode == GESTURE_MODE_IDLE)  emit motionDetected();
                        m_privmode = GESTURE_MODE_GESTURE;
                    {
                        Gesture_gesture_type gesture_type = (Gesture_gesture_type)((ba[5] & GESTURE_TYPE_BIT_MASK) >> GESTURE_TYPE_BIT_SHIFT);
                        switch(gesture_type){
                        case GESTURE_TYPE_UP_TO_DOWN: emit downGesture(); break;
                        case GESTURE_TYPE_RIGHT_TO_LEFT: emit rightGesture(); break;
                        default: //qDebug() << "unrecognized gesture" << gesture_type;
                            break;
                        }
                    }
                        break;
                        // default: qDebug() << "Unrecognized mode received" << mode_info;
                        break;
                    }
                    }
                }
            }
            msleep(1);
        }
    }
    emit srtQuit();
    qDebug() << "Serial reader thread quit\n";
}
